all:
	make -s verify
	make -s lint-html

dependencies:
	npm install

deploy:
	./deploy.sh

verify:
	npx prettier . --write

lint-html:
	java -jar ./node_modules/vnu-jar/build/dist/vnu.jar --errors-only *.html
