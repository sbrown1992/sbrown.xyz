HOST=65.108.145.113

scp index.html sbrown@${HOST}:/var/www/sbrown.xyz/html/index.html
scp nginx-site.conf sbrown@${HOST}:/etc/nginx/sites-available/sbrown.xyz

ssh -t sbrown@${HOST} 'sudo service nginx restart'
